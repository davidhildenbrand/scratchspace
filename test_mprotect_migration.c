/*
 * Simple program for testing if concurent page migration can affect the
 * outcome of mprotect(), accidentially allowing for write access to a
 * page mprotect(PROT_READ) completed.
 *
 * This program assumes that node 0 and 1 are around and have memory. While
 * running this program, there should be a fast increase in:
 * 	# cat /proc/vmstat  | grep pgmigrate_success
 *
 * Some random page migration errors can be ignored.
 *
 * Author(s): David Hildenbrand <david@redhat.com>
 */
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/mman.h>
#include <setjmp.h>
#include <pthread.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <linux/mempolicy.h>

static sigjmp_buf env;
static char *mem;
static size_t pagesize;

static void *migration_thread_fn(void *arg)
{
	void *page = mem;
	int node = 0;

	while (true) {
		int status = -1;

		/*
		 * We need two nodes (0, 1), so we can migrate back and forth.
		 * With mbind() it would be easier, however, mbind() always
		 * migrates pages while holding the mmap lock in write mode:
		 * preventing concurrent mprotect() to race.
		 *
		 * move_pages(), only holds the mmap lock in read mode while
		 * collecting pages, migrate_pages() is even called without
		 * holding the mmap lock.
		 */
		node = !node;

		if (syscall(__NR_move_pages, getpid(), 1, &page, &node, &status,
			    MPOL_MF_MOVE))
			perror("move_pages() failed");
		else if (status != node)
			fprintf(stderr, "page not migrated\n");
	}
	return NULL;
}

static void signal_handler(int sig)
{
	if (sig == SIGSEGV)
		siglongjmp(env, 1);
	siglongjmp(env, 2);
}

int main(void)
{
	pthread_t migration_thread;
	int ret;

	pagesize = getpagesize();
	mem = mmap(NULL, pagesize, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON,
		   -1, 0);
	if (mem == MAP_FAILED) {
		perror("mmap() failed");
		return EXIT_FAILURE;
	}
	memset(mem, 1, pagesize);

	/* Keep migrating the page around ... */
	pthread_create(&migration_thread, NULL, migration_thread_fn, NULL);

	if (signal(SIGSEGV, signal_handler) == SIG_ERR) {
		perror("signal() failed");
		return EXIT_FAILURE;
	}

	while (true) {
		/* We might race with page migration. */
		if (mprotect(mem, pagesize, PROT_READ)) {
			perror("mprotect() failed");
			return EXIT_FAILURE;
		}

		/* Make sure, mprotect() succeeded in wr-protecting the page. */
		ret = sigsetjmp(env, 1);
		if (!ret)
			*mem = 2;
		if (ret != 1 || *mem != 1) {
			fprintf(stderr, "SIGSEGV not triggered");
			return EXIT_FAILURE;
		}

		/* Restore write permissions and map the page writable. */
		if (mprotect(mem, pagesize, PROT_READ|PROT_WRITE)) {
			perror("mprotect() failed\n");
			return EXIT_FAILURE;
		}
		*mem = 1;
	}
	return 0;
}
