/*
 * This is a simply, race-free testcase for a problem discovered by
 * Andrea Arcangelli, summarized in:
 *	https://lore.kernel.org/r/3ae33b08-d9ef-f846-56fb-645e3b9b4c66@redhat.com
 *
 * "
 *   2. Intra Process Memory Corruptions due to Wrong COW (FOLL_GET)
 *
 *   It was discovered that we can create a memory corruption by reading a
 *   file via O_DIRECT to a part (e.g., first 512 bytes) of a page,
 *   concurrently writing to an unrelated part (e.g., last byte) of the same
 *   page, and concurrently write-protecting the page via clear_refs
 *   SOFTDIRTY tracking [6].
 *
 *   For the reproducer, the issue is that O_DIRECT grabs a reference of the
 *   target page (via FOLL_GET) and clear_refs write-protects the relevant
 *   page table entry. On successive write access to the page from the
 *   process itself, we wrongly COW the page when resolving the write fault,
 *   resulting in a loss of synchronicity and consequently a memory corruption.
 *
 *   While some people might think that using clear_refs in this combination
 *   is a corner cases, it turns out to be a more generic problem unfortunately.
 *
 *   For example, it was just recently discovered that we can similarly
 *   create a memory corruption without clear_refs, simply by concurrently
 *   swapping out the buffer pages [7]. Note that we nowadays even use the
 *   swap infrastructure in Linux without an actual swap disk/partition: the
 *   prime example is zram which is enabled as default under Fedora [10].
 *
 *   The root issue is that a write-fault on a page that has additional
 *   references results in a COW and thereby a loss of synchronicity
 *   and consequently a memory corruption if two parties believe they are
 *   referencing the same page.
 * "
 *
 * This test case verifies that a page with additional references (via
 * vmsplice()) won't get accidentially replaced by a copy due to COW when
 * the page was faulted back in from the swapcache after it was swapped out
 * and the PTE was replaced by a swap PTE.
 *
 * Swap space is required for this test, otherwise swapout will fail early.
 *
 * Notes:
 * * vmsplice() is used to not having to rely on O_DIRECT race windows
 *   to reproduce detect the COW replacement. But the effect observed is the
 *   same: the page in the page table got replaced by a copy.
 * * This applies only to FOLL_GET users that really should be using
 *   FOLL_PIN instead (O_DIRECT, vmsplice()). Once they properly use FOLL_PIN,
 *   this test case might always abort early, failing to swapout a page that
 *   is pinned.
 * * In contrast to FOLL_PIN, fork() has to be left out of the picture. With
 *   FOLL_GET, we only want to guarantee that an exclusive anonymous page
 *   stays exclusive, even when swapped out and swapped back in.
 *
 * Copyright (C) 2022  Red Hat, Inc.
 * Author: David Hildenbrand <david@redhat.com>
 */
#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>

static size_t pagesize;
static int pagemap_fd;

# define barrier() __asm__ __volatile__("": : :"memory")

static uint64_t pagemap_get_entry(int fd, char *start)
{
	const unsigned long pfn = (unsigned long)start / getpagesize();
	uint64_t entry;
	int ret;

	ret = pread(fd, &entry, sizeof(entry), pfn * sizeof(entry));
	if (ret != sizeof(entry)) {
		fprintf(stderr, "Reading /proc/self/pagemap failed\n");
		exit(1);
	}
	return entry;
}

static bool pagemap_is_swapped(int fd, char *start)
{
	uint64_t entry = pagemap_get_entry(fd, start);

	return entry & 0x4000000000000000ull;
}

volatile char tmp;

int main(void)
{
	struct iovec iov;
	int fds[2];
	char *mem;

	pagesize = getpagesize();
	pagemap_fd = open("/proc/self/pagemap", O_RDONLY);
	if (pagemap_fd < 0) {
		fprintf(stderr, "Opening /proc/self/pagemap failed\n");
		return 1;
	}

	mem = mmap(NULL, pagesize, PROT_READ | PROT_WRITE,
		   MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (mem == MAP_FAILED) {
		fprintf(stderr, "mmap() failed\n");
		return 1;
	}
	if (madvise(mem, pagesize, MADV_NOHUGEPAGE) && errno != EINVAL) {
		fprintf(stderr, "MADV_NOHUGEPAGE failed\n");
		return 1;
	}

	/* Populate an exclusive anonymous page. */
	memset(mem, 0, pagesize);

	if (pipe(fds) < 0) {
		fprintf(stderr, "pipe() failed\n");
		return 1;
	}

	/*
	 * vmsplice() it, this will fault in the page again and grab a GUP
	 * reference. In the future (once O_DIRECT uses FOLL_PIN) this would
	 * properly long-term pin the page.
	 */
	iov.iov_base = mem;
	iov.iov_len = pagesize;
	if (vmsplice(fds[1], &iov, 1, 0) <= 0) {
		fprintf(stderr, "vmsplice() failed\n");
		return 1;
	}

	/*
	 * Swap it out. In the future (once O_DIRECT uses FOLL_PIN,
	 * properly pinning the page), this might fail (which would be valid).
	 * For now, this should work. After this call, the page should be in
	 * the swap cache and referenced via a swap PTE from the page table.
	 */
	madvise(mem, pagesize, MADV_PAGEOUT);
	if (!pagemap_is_swapped(pagemap_fd, mem)) {
		fprintf(stderr, "MADV_PAGEOUT did not work\n");
		return 1;
	}

	/* Trigger a read fault. */
	tmp = *mem;
	barrier();
	/* Trigger a write fault. */
	memset(mem, 1, pagesize);

	/*
	 * Note: this is *not* "desired" vmsplice() behavior, but it's been the
	 * behavior under Linux probably forever: modifications to the page
	 * after vmsplice() are visible when reading from the pipe.
	 */
	while (true) {
		ssize_t cur = read(fds[0], (void *)&tmp, 1);

		if (cur < 0) {
			fprintf(stderr, "read() failed\n");
			return 1;
		} else if (cur == 1) {
			break;
		}
	}

	if (tmp == 0) {
		printf("FAIL: page was replaced during COW\n");
		return 1;
	}
	printf("PASS: page was not replaced during COW\n");
	return 0;
}
