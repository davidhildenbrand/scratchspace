/*
 * Simple reproducer that highlights why using the reference count instead
 * of the mapcount when making the decision wheter a write-fault has to
 * result in a Copy On Write (COW) is bogus.
 *
 * The R/O long-term pin loses synchronicity with the page tables of the
 * MM, resulting in data read via the R/O long-term pinning being stale and
 * consuming additional memory.
 *
 *  Copyright (C) 2021  Red Hat, Inc.
 *  Author: David Hildenbrand <david@redhat.com>
 */
#define _GNU_SOURCE
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <err.h>
#include <unistd.h>
#include <sys/mman.h>

int main(void)
{
	unsigned long pagesize;
	unsigned char *mem, *buf;
	struct iovec iov;
	int pipe_fds[2];
	pid_t pid;

	pagesize = sysconf(_SC_PAGE_SIZE);
	buf = malloc(pagesize);

	/* Map our target page. */
	mem = mmap(NULL, pagesize, PROT_READ | PROT_WRITE,
		   MAP_ANON | MAP_PRIVATE, -1, 0);
	if (mem == MAP_FAILED) {
		perror("mmap() failed");
		return -errno;
	}

	/* Force no huge pages */
	if (madvise(mem, pagesize, MADV_NOHUGEPAGE)) {
		perror("madvise() failed");
		return -errno;
	}

	/* populate the page */
	memset(mem, 0, pagesize);

	/*
	 * Map it write-protected. There are various ways, including automatic
	 * ones like swap + read access.
	 */
	pid = fork();
	if (pid < 0) {
		perror("fork() failed");
		exit(-errno);
	} else if (pid == 0) {
		/* Do nothing. */
		return 0;
	}
	assert(mem[0] == 0);
	assert(mem[pagesize - 1] == 0);

	/* Setup vmsplice, or any other FOLL_LONGTERM R/O like RDMA. */
	if (pipe(pipe_fds) < 0) {
		perror("pipe() failed");
		return -errno;
	}
	iov.iov_base = mem;
	iov.iov_len = pagesize;

	if (vmsplice(pipe_fds[1], &iov, 1, 0) < 0) {
		perror("vmsplice() failed");
		return -errno;
	}

	/* Modify our page, this will trigger a write fault. */
	memset(mem, 0xff, pagesize);

	/* Let's see what we read via oure long-term GUP. */
	if (read(pipe_fds[0], buf, pagesize) != pagesize) {
		perror("read() failed");
		return -errno;
	}
	assert(buf[0] == 0xff);
	assert(buf[pagesize - 1] == 0xff);
}
