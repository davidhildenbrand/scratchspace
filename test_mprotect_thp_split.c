/*
 * Simple program for testing if concurent THP splitting can affect the
 * outcome of mprotect(), accidentially allowing for write access to a
 * page after mprotect(PROT_READ) completed.
 *
 * Author(s): David Hildenbrand <david@redhat.com>
 */
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <setjmp.h>
#include <pthread.h>

static size_t thpsize = 2 * 1024 * 1024u;

static sigjmp_buf env;
static int split_fd;

static void *split_thread_fn(void *arg)
{
	while (true) {
		/*
		 * split_huge_pages_all() will split all THPs in the system
		 * without taking the mmap lock. So we can race against
		 * mprotect().
		 */
		if (write(split_fd, "1", 1) != 1) {
			perror("write() failed");
			exit(EXIT_FAILURE);
		}
	}
	return NULL;
}

static void signal_handler(int sig)
{
	if (sig == SIGSEGV)
		siglongjmp(env, 1);
	siglongjmp(env, 2);
}

int main(void)
{
	pthread_t split_thread;
	static char *mem;
	int ret;

	split_fd = open("/sys/kernel/debug/split_huge_pages", O_RDWR);
	if (split_fd < 0) {
		perror("open(\"/sys/kernel/debug/split_huge_pages\") failed");
		return EXIT_FAILURE;
	}

	mem = mmap(NULL, 2 * thpsize, PROT_READ|PROT_WRITE,
		   MAP_PRIVATE|MAP_ANON, -1, 0);
	if (mem == MAP_FAILED) {
		perror("mmap() failed");
		return EXIT_FAILURE;
	}
	/* We need a THP-aligned memory area. */
	mem = (char *)(((uintptr_t)mem + thpsize) & ~(thpsize - 1));

	if (madvise(mem, thpsize, MADV_HUGEPAGE)) {
		perror("madvise() failed");
		return EXIT_FAILURE;
	}
	/* Try populating a THP. */
	*mem = 1;

	/* Keep splitting all THP in the system ... */
	pthread_create(&split_thread, NULL, split_thread_fn, NULL);

	if (signal(SIGSEGV, signal_handler) == SIG_ERR) {
		perror("signal() failed");
		return EXIT_FAILURE;
	}

	while (true) {
		/* We might race with THP splitting. */
		if (mprotect(mem, thpsize, PROT_READ)) {
			perror("mprotect() failed");
			return EXIT_FAILURE;
		}

		/* Make sure, mprotect() succeeded in wr-protecting the page. */
		ret = sigsetjmp(env, 1);
		if (!ret)
			*mem = 2;
		if (ret != 1 || *mem != 1) {
			fprintf(stderr, "SIGSEGV not triggered");
			return EXIT_FAILURE;
		}

		/*
		 * Remap the memory (removing any leftovers from THP splitting),
		 * so we can get a fresh THP.
		 */
		mem = mmap(mem, thpsize, PROT_READ|PROT_WRITE,
			   MAP_PRIVATE|MAP_ANON|MAP_FIXED, -1, 0);
		if (mem == MAP_FAILED) {
			perror("mmap() failed");
			return EXIT_FAILURE;
		}

		if (madvise(mem, thpsize, MADV_HUGEPAGE)) {
			perror("madvise() failed");
			return EXIT_FAILURE;
		}
		/* Try populating a THP. */
		*mem = 1;
	}
	return 0;
}
