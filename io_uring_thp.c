// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Reproducer to show how FOLL_LONGTERM as used bi io_uring can be used
 * to permananetly block a physical area from getting compacted into a
 * THP again. All THPs blocked by this process will remain blocked until the
 * process dies.
 *
 * The process will always consume ~ a single THP + the memlock limit
 * of anonymous pages at a time.
 *
 * The idea is to get a THP populated, to trigger split it (the PMD map and
 * the compound page), to pin a single page of the split THP, and to keep
 * the page pinned for all eternity. As the single pinned page cannot get
 * migrated/swapped, the THP is essentially lost until the process quits.
 *
 * A recent version of liburing is required.
 *
 * $ gcc -O2 -o io_uring_thp io_uring_thp.c -Wall -luring
 *
 * With a custom liburing build:
 *
 * $ gcc -O2 -I ../liburing/src/include/ -L ../liburing/src/ -o io_uring_thp io_uring_thp.c -Wall -luring
 *
 * Note: Requires a recent kernel (e.g., v5.14) with THP enabled for madvise
 * or all mmaps.
 *
 *  Copyright (C) 2021  Red Hat, Inc.
 *  Author: David Hildenbrand <david@redhat.com>
 */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include "liburing.h"

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

static unsigned long pagesize;

static uint64_t pagemap_get_entry(int fd, char *start)
{
	const unsigned long pfn = (unsigned long)start / pagesize;
	uint64_t entry;
	int ret;

	ret = pread(fd, &entry, sizeof(entry), pfn * sizeof(entry));
	if (ret != sizeof(entry)) {
		perror("pread");
		exit(-errno);
	}
	return entry;
}

static bool pagemap_is_populated(char *start)
{
	int fd = open("/proc/self/pagemap", O_RDONLY);
	uint64_t entry;

	if (fd < 0) {
		perror("open");
		exit(-errno);
	}
	entry = pagemap_get_entry(fd, start);
	close(fd);

	/* Present or swapped. */
	return entry & 0xc000000000000000ull;
}

static unsigned long detect_thp_size(void)
{
	int fd = open("/sys/kernel/mm/transparent_hugepage/hpage_pmd_size", O_RDONLY);
	unsigned long val;
	char buf[80 + 1];
	int ret;

	if (fd < 0) {
		fprintf(stderr, "Assuming 2 MiB THP\n");
		return 2 * 1024 * 1024u;
	}

	ret = pread(fd, buf, sizeof(buf) - 1, 0);
	if (ret <= 0) {
		fprintf(stderr, "Assuming 2 MiB THP\n");
		val = 2 * 1024 * 1024u;
	} else {
		buf[ret] = 0;
		val = strtoul(buf, NULL, 10);
	}
	close(fd);

	return val;
}

int main(void)
{
	unsigned long max_buffers, thp_size, pages_per_thp;
	unsigned long blocked_thp_per_user, blocked_thp_per_process;
	struct io_uring ring;
	struct rlimit rlim;
	struct iovec *iovs;
	char *area;
	int ret, i;

	pagesize = sysconf(_SC_PAGE_SIZE);
	printf("PAGE size: %lu bytes (sensed)\n", pagesize);

	thp_size = detect_thp_size();
	printf("THP size: %lu bytes (sensed)\n", thp_size);

	ret = getrlimit(RLIMIT_MEMLOCK, &rlim);
	if (ret) {
		perror("getrlimit");
		return -errno;
	}

	printf("RLIMIT_MEMLOCK: %lu bytes (sensed)\n", rlim.rlim_cur);

	/*
	 * New kernels support 1<<14 buffers, older ones 1024. Let's
	 * assume we're running on a recent kernel and not implement
	 * fallback logic.
	 */
	max_buffers = 1u << 14;
	printf("IORING_MAX_REG_BUFFERS: %lu (guess)\n", max_buffers);

	pages_per_thp = thp_size / pagesize;
	if (pages_per_thp <= 1) {
		fprintf(stderr, "pages_per_thp <= 1");
		return -EINVAL;
	}
	printf("Pages per THP: %lu\n", pages_per_thp);

	/* With each FOLL_LONGTERM 4k page, we can block one THP. */
	blocked_thp_per_user = rlim.rlim_cur / pagesize;
	printf("User can block %lu THP (%lu bytes)\n", blocked_thp_per_user,
	       blocked_thp_per_user * thp_size);

	/*
	 * With each buffer, we can block one THP. Actually, we could cross THPs
	 * and block more, but then we "waste" more of out memlock limit.
	 */
	blocked_thp_per_process = max_buffers;
	blocked_thp_per_process = MIN(blocked_thp_per_process,
				      blocked_thp_per_user);
	printf("Process can block %lu THP (%lu bytes)\n", blocked_thp_per_process,
	       blocked_thp_per_process * thp_size);

	/* We need a THP-aligned area such that we can get THPs. */
	area = mmap(0, 2 * thp_size, PROT_READ | PROT_WRITE, MAP_ANONYMOUS |
		    MAP_PRIVATE, -1, 0);
	if (area == MAP_FAILED) {
		perror("mmap");
		return -errno;
	}
	area += thp_size - ((uintptr_t)area & (thp_size - 1));

	/*
	 * Prepare all empty IOVs we could possibly use and register
	 * them. We'll update them as we go.
	 */
	iovs = malloc(blocked_thp_per_process * sizeof(*iovs));
	memset(iovs, 0, blocked_thp_per_process * sizeof(*iovs));

	ret = io_uring_queue_init(1, &ring, 0);
	if (ret < 0) {
		perror("io_uring_queue_init");
		return ret;
	}

	ret = io_uring_register_buffers(&ring, iovs, blocked_thp_per_process);
	if (ret) {
		fprintf(stderr, "io_uring_register_buffers() failed: %d\n",
			ret);
		return ret;
	}

	/*
	 * We'll do it using a single mmap with a single THP at a time and
	 * use the buffer update feature. We could use a larger mmap and
	 * simply unregister+re-register the buffers, but this
	 * way is even neater, because our process looks somewhat harmless.
	 */
	for (i = 0; i < blocked_thp_per_process; i++, printf("Blocking %u THP\n", i)) {
		char *new_area;

retry:
		/*
		 * Remove the page table as well such that we can easily place
		 * a THP again. This currently requires an unmap+mmap because
		 * MADV_DONTNEED won't remove the page table yet.
		 */
		new_area = mmap(area, thp_size, PROT_READ | PROT_WRITE, 
				MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED, -1, 0);
		if (area != new_area) {
			fprintf(stderr, "mmap(MAP_FIXED)\n");
			break;
		}

		/* We most certainly want THPs. */
		ret = madvise(area, thp_size, MADV_HUGEPAGE);
		if (ret) {
			perror("madvise(MADV_HUGEPAGE)");
			return -ret;
		}

		/* Touch the first page, cross fingers that we get a THP. */
		area[0] = 0;

		if (!pagemap_is_populated(area)) {
			fprintf(stderr, "Population error!\n");
			break;
		}
		if (!pagemap_is_populated(area + thp_size - pagesize)) {
			sleep(1);
			goto retry;
		}

		/*
		 * Split the PMD mapping into PTE mapping and split the compound
		 * page. As an alternative, we could do an MADV_DONTNEED of
		 * a single sub-page and wait until it was deferred split by the
		 * system.
		 *
		 * This is required to be able to pin a single 4k page instead
		 * of the complete compound page.
		 */
		ret = madvise(area + thp_size - pagesize, pagesize,
			      MADV_FREE);
		if (ret) {
			perror("madvise(MADV_FREE)");
			sleep(1);
			goto retry;
		}

		iovs[i].iov_base = area;
		iovs[i].iov_len = pagesize;

		/*
		 * Update the single buffer, resulting in us pinning
		 * a 4k page that once belonged to a THP.
		 */
		ret = io_uring_register_buffers_update_tag(&ring, i, iovs,
							   NULL, 1);
		if (ret < 0) {
			fprintf(stderr, "io_uring_register_buffers_update_tag: %d\n",
				ret);
			break;
		}

		/*
		 * zap the range completely. Only the pinned 4k page will
		 * consume memory.
		 */
		ret = madvise(area, thp_size, MADV_DONTNEED);
		if (ret) {
			perror("madvise(MADV_HUGEPAGE)");
			sleep(1);
			goto retry;
		}
	}

	pause();
	return 0;
}
