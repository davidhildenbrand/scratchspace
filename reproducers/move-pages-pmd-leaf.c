/*
 * Reproducer for a move_pages() pmd_leaf() issue with migration entries:
 *   https://lkml.kernel.org/r/20241015111236.1290921-1-david@redhat.com
 *
 * Requires a x86-64 machine (preferably VM) with at least two NUMA nodes.
 *
 * # echo 2 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
 * # gcc move-pages-pmd-leaf.c -o move-pages-pmd-leaf -O3 -lnuma -lpthread
 * # ./move-pages-pmd-leaf
 *
 * Copyright (C) 2024  Red Hat, Inc.
 *
 * Author(s): David Hildenbrand <david@redhat.com>
 */
#include <stdio.h>
#include <sys/mman.h>
#include <linux/mman.h>
#include <errno.h>
#include <string.h>
#include <numaif.h>
#include <pthread.h>

#define SIZE_2MB (2 *1024 * 1024ul)

static void migrate_loop(char *mem)
{
        unsigned int i;

        /* Migrate it back and forth between two nodes. */
        for (i = 0; ; i++) {
                void *pages[1] = { mem, };
                int nodes[1] = { i % 2, };
                int status[1] = { 0 };

                if (move_pages(0, 1, pages, nodes, status, MPOL_MF_MOVE_ALL))
                        fprintf(stderr, "move_pages failed: %d\n", errno);
                if (status[0] != nodes[0] && status[0] != -ENOENT && status[0] != -EBUSY)
                        printf("migration failed: wanted %d, is %d\n", nodes[0], status[0]);
        }
}

static void *migrate_thread_fn(void *arg)
{
        char *mem = arg;

        migrate_loop(mem);
}

int main(void)
{
        pthread_t migrate_thread;
        char *mem;

        mem = mmap(0, SIZE_2MB, PROT_READ|PROT_WRITE,
                   MAP_ANON|MAP_PRIVATE|MAP_HUGETLB|MAP_HUGE_2MB, -1, 0);
        if (mem == MAP_FAILED) {
                fprintf(stderr, "mmap() failed: %d\n", errno);
                return -1;
        }

        memset(mem, 0, SIZE_2MB);

        pthread_create(&migrate_thread, NULL, migrate_thread_fn, mem);
        migrate_loop(mem);
        return 0;
}
