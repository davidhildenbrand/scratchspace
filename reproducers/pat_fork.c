/*
 * Reproducer for a VM_PAT issue when fork() fails.
 *   https://lore.kernel.org/lkml/CABOYnLx_dnqzpCW99G81DmOr+2UzdmZMk=T3uxwNxwz+R1RAwg@mail.gmail.com/
 *
 * Requires an x86_64 machine with /dev/mem access and
 * * CONFIG_FAULT_INJECTION=y
 * * CONFIG_FAILSLAB=y
 * * CONFIG_FAIL_PAGE_ALLOC=y
 * * CONFIG_FAULT_INJECTION_DEBUG_FS=y
 *
 * # pat_fork.c -o pat_fork
 * # ./pat_fork
 *
 * Copyright (C) 2024  Red Hat, Inc.
 *
 * Author(s): David Hildenbrand <david@redhat.com>
 */
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>

static int setup_failure_injection(int fail_fd, int n)
{
	char buf[16];

	sprintf(buf, "%d", n);
	if (pwrite(fail_fd, buf, strlen(buf), 0) != (ssize_t)strlen(buf))
		return 1;
	return 0;
}

int main(void)
{
	size_t pagesize = getpagesize();
	int mem_fd, fail_fd, i, ret;
	char *map;

	mem_fd = open("/dev/mem", O_RDONLY);
	if (mem_fd < 0) {
		perror("open /dev/mem failed");
		return 1;
	}
	fail_fd = open("/proc/self/fail-nth", O_RDWR);
	if (fail_fd < 0) {
		perror("open fail-nth failed");
		return 1;
	}

	/* Make most allocations fail. */
	system("echo N > /sys/kernel/debug/failslab/ignore-gfp-wait");
	system("echo N > /sys/kernel/debug/fail_page_alloc/ignore-gfp-wait");
	system("echo N > /sys/kernel/debug/fail_page_alloc/ignore-gfp-highmem");
	system("echo 0 > /sys/kernel/debug/fail_page_alloc/min-order");

	/* No output except the warning we are looking for. */
	system("echo 0 > /sys/kernel/debug/fail_page_alloc/verbose");
	system("echo 0 > /sys/kernel/debug/failslab/verbose");
	system("echo 0 > /sys/kernel/debug/fail_usercopy/verbose");

	/* 10000 should be sufficient to trigger most issues ... */
	for (i = 1; i < 10000; i++) {
		map = mmap(0, pagesize, PROT_READ, MAP_SHARED, mem_fd, 0);
		if (map == MAP_FAILED) {
			perror("mmap failed");
			return 1;
		}

		if (setup_failure_injection(fail_fd, i)) {
			perror("Could not prepare alloc failure");
			return 1;
		}

		ret = fork();
		if (ret == 0)
			return 0;

		/* Disbale fault injection so the other syscalls won't fail. */
		while (setup_failure_injection(fail_fd, 0))
			;

		/* Wait if fork() succeeded. */
		if (ret > 0)
			wait(NULL);

		munmap(map, pagesize);
	}
	return 0;
}
