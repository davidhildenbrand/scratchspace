/*
 * Reproducer for a hugetlb issue, whereby we could get a writable PTE
 * in a VMA without VM_WRITE (PROT_NONE).
 *
 * Requires a x86-64 machine.
 *
 * # echo 2 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
 * # gcc hugetlb-mkwrite-fork.c -o hugetlb-mkwrite-fork -O3 -luring
 * # ./hugetlb-mkwrite-fork
 *
 * Copyright (C) 2024  Red Hat, Inc.
 *
 * Author(s): David Hildenbrand <david@redhat.com>
 */
#define _GNU_SOURCE
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <linux/mman.h>
#include <sys/wait.h>
#include <liburing.h>

static const size_t hugetlbsize = 2 * 1024 * 1024ul;

static void signal_handler(int sig)
{
	 fprintf(stderr, "[PASS] access did not work as expected\n");
	 exit(0);
}

int main(void)
{
	struct io_uring ring;
	struct iovec iov;
	int ret, status;
	char *mem;

	mem = mmap(NULL, hugetlbsize, PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB | MAP_HUGE_2MB,
			-1, 0);
	if (mem == MAP_FAILED) {
		perror("mmap() failed");
		return -1;
	}

	memset(mem, 0, hugetlbsize);

	/* Register a fixed buffer (FOLL_WRITE|FOLL_PIN|FOLL_LONGTERM). */
	ret = io_uring_queue_init(1, &ring, 0);
	if (ret < 0) {
		perror("io_uring_queue_init() failed");
		return -1;
	}
	iov.iov_base = mem;
	iov.iov_len = hugetlbsize;
	ret = io_uring_register_buffers(&ring, &iov, 1);
	if (ret) {
		perror("io_uring_register_buffers() failed");
		return -1;
	}

	/* Disallow write access. */
	if (mprotect(mem, hugetlbsize, PROT_READ)) {
		perror("mprotect() failed");
		return -1;
	}

	/* Create a child. As the page is pinned, we'll get a copy. */
	ret = fork();
	if (ret < 0) {
		perror("fork() failed");
		return -1;
	} else if (!ret) {
		if (signal(SIGSEGV, signal_handler) == SIG_ERR) {
			perror("signal() failed");
			return -1;
		}

		memset(mem, 1, hugetlbsize);
		fprintf(stderr, "[FAIL] access should not have worked\n");
		return 0;
	}

	waitpid(ret, &status, 0);

	return 0;
}
