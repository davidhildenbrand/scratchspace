/*
 * Reproducer for a hugetlb vs. core-mm PT locking issue:
 *   https://lkml.kernel.org/r/20240725183955.2268884-1-david@redhat.com
 *
 * Requires an aarch64 machine (preferably VM) with at least two NUMA nodes.
 *
 * # echo 100  > /sys/kernel/mm/hugepages/hugepages-64kB/nr_hugepages
 * # gcc hugetlb-ptlock.c -o hugetlb-ptlock -O3 -lnuma -lpthread
 * # ./hugetlb-ptlock
 *
 * Copyright (C) 2024  Red Hat, Inc.
 *
 * Author(s): David Hildenbrand <david@redhat.com>
 */
#include <stdio.h>
#include <sys/mman.h>
#include <linux/mman.h>
#include <errno.h>
#include <string.h>
#include <numaif.h>
#include <pthread.h>

#define SIZE_64KB (64 * 1024ul)

static void *thread_fn(void *arg)
{
	char *mem = arg;

	/* Let GUP go crazy on the page without grabbing a reference. */
	while (1) {
		if (madvise(mem, SIZE_64KB, MADV_POPULATE_WRITE)) {
			fprintf(stderr, "madvise() failed: %d\n", errno);
		}
	}
}

int main(void)
{
	pthread_t thread;
	unsigned int i;
	char *mem;

	mem = mmap(0, SIZE_64KB, PROT_READ|PROT_WRITE,
		   MAP_ANON|MAP_PRIVATE|MAP_HUGETLB|MAP_HUGE_64KB, -1, 0);
	if (mem == MAP_FAILED) {
		fprintf(stderr, "mmap() failed: %d\n", errno);
		return -1;
	}

	memset(mem, 0, SIZE_64KB);

	pthread_create(&thread, NULL, thread_fn, mem);

	/* Migrate it back and forth between two nodes. */
	for (i = 0; ; i++) {
		void *pages[1] = { mem, };
		int nodes[1] = { i % 2, };
		int status[1] = { 0 };

		if (move_pages(0, 1, pages, nodes, status, MPOL_MF_MOVE_ALL))
			fprintf(stderr, "move_pages failed: %d\n", errno);
		if (status[0] != nodes[0])
			printf("migration failed\n");
	}
	return 0;
}
