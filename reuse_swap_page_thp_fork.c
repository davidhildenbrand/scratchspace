/*
 * More complicated reproducer (based on gup_test) that gets a subpage of a
 * THP mapped R/O with page_trans_huge_map_swapcount() == 0 and has it
 * transitioned to page_trans_huge_map_swapcount() > 1 without the usage
 * of fork() in that process, leading to page pinning issues.
 *
 * Copyright (C) 2023  Red Hat, Inc.
 * Author(s): David Hildenbrand <david@redhat.com>
 */
#define _GNU_SOURCE
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <sys/signal.h>

#include <linux/types.h>
#define PIN_LONGTERM_TEST_START	_IOW('g', 7, struct pin_longterm_test)
#define PIN_LONGTERM_TEST_STOP	_IO('g', 8)
#define PIN_LONGTERM_TEST_READ	_IOW('g', 9, __u64)

#define PIN_LONGTERM_TEST_FLAG_USE_WRITE	1
#define PIN_LONGTERM_TEST_FLAG_USE_FAST		2

struct pin_longterm_test {
	__u64 addr;
	__u64 size;
	__u32 flags;
};

static const size_t thpsize = 2 * 1024 * 1024ul;
size_t pagesize;

int main(void)
{
	struct pin_longterm_test args;
	char *mem, *mmap_mem, *tmp;
	size_t mmap_size;
	int ret, gup_fd;
	uint64_t tmp_val;
	int i;

	pagesize = getpagesize();
	gup_fd = open("/sys/kernel/debug/gup_test", O_RDWR);
	if (gup_fd < 0) {
		perror("gup_test not available");
		return -1;
	}

	/* We need a THP-aligned memory area. */
	mmap_size = 2 * thpsize;
	mmap_mem = mmap(NULL, mmap_size, PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (mmap_mem == MAP_FAILED) {
		perror("mmap() failed");
		return -1;
	}
	mem = (char *)(((uintptr_t)mmap_mem + thpsize) & ~(thpsize - 1));

	/* Enable THP. */
	if (madvise(mem, thpsize, MADV_HUGEPAGE)) {
		perror("madvise() failed");
		return -1;
	}

	/* Let's hope we get a THP populated. */
	memset(mem, 0, thpsize);

	ret = fork();
	if (ret < 0) {
		perror("fork() failed");
		return -1;
	} else if (!ret) {
		prctl(PR_SET_PDEATHSIG, SIGTERM);

		sleep(1); /* STEP 1: THP is shared */

		/*
		 * Overwrite the first subpage. As an alternative, we could
		 * unmap.
		 */
		memset(mem, 2, pagesize);

		sleep(1); /* STEP 2: each subpage is only mapped by one process */

		sleep(1); /* STEP 3: parent pinned its subpages. */

		ret = fork();
		if (ret < 0) {
			perror("fork() failed");
			return -1;
		} else if (!ret) {
			prctl(PR_SET_PDEATHSIG, SIGTERM);
			pause();
			exit(0);
		}

		sleep(1); /* STEP 4: child shared its subpages. */
		pause();
		exit(0);
	}

	/*
	 * Note: The THP is now mapped R/O and completely shared with the child
	 * process.
	 */

	sleep(1); /* STEP 1: THP is shared */

	/*
	 * Overwrite all but the first subpage. As an alternative, we could
	 * unmap.
	 */
	memset(mem + pagesize, 0, thpsize - pagesize);

	sleep(1); /* STEP 2: each subpage is only mapped by one process */

	/*
	 * Note: each subpage is only mapped by one process, so
	 * page_trans_huge_map_swapcount(subpage) == 1.
	 */

	/*
	 * Take a FOLL_LONGTERM pin a (now detected as exclusive) subpage.
	 * We could also take a R/W pin, but would have to get the page mapped
	 * R/O again to make it trigger.
	 */
	args.addr = (uint64_t)(uintptr_t)mem;
	args.size = pagesize;
	args.flags = 0;
	ret = ioctl(gup_fd, PIN_LONGTERM_TEST_START, &args);
	if (ret) {
		perror("PIN_LONGTERM_TEST_START failed");
		return -1;
	}

	sleep(1); /* STEP 3: parent pinned the subpages. */

	/*
	 * Note that the child of the child won't get one of its (previously
	 * exclusive to the child) subpages copied, because the child never
	 * pinned pages: page_needs_cow_for_dma() always returns false in the
	 * child when fork()'ing.
	 */

	sleep(1); /* STEP 4: child shared its subpages. */

	/* Now modify the subpage and see if we observe the change via our pin. */
	memset(mem, 1, pagesize);

	tmp = malloc(pagesize);
	tmp_val = (uint64_t)(uintptr_t)tmp;
	ret = ioctl(gup_fd, PIN_LONGTERM_TEST_READ, &tmp_val);
	ioctl(gup_fd, PIN_LONGTERM_TEST_STOP);
	if (ret) {
		perror("PIN_LONGTERM_TEST_READ failed\n");
		return -1;
	}

	for (i = 0; i < pagesize; i++) {
		if (tmp[i] != 1) {
			fprintf(stderr,
				"[FAIL] Longterm pin is unreliable, read '%d'\n",
				tmp[i]);
			return -1;
		}
	}
	printf("[PASS] Longterm pin is reliable\n");
	return 0;
}
